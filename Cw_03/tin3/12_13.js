function Student(index, name, lastName, marks) {
    this.index = index;
    this.name = name;
    this.lastName = lastName;
    this.marks = marks;
}

Student.prototype.listOfClasses = ['tin1', 'tin2', 'tin3'];
Student.prototype.avg = function() {
    sum = 0;
    this.marks.forEach((m) => sum += m);
    return sum / this.marks.length;
};
Student.prototype.fullName = function() {
    return this.name + ' ' + this.lastName;
}

function describe(object) {
    Object.keys(object).forEach((key) => {
        console.log(key, typeof object[key]), object[key];
        if (typeof object[key] == 'function') {
            console.log('   --->', object[key]());
        } else {
            console.log('   --->', object[key]);
        }
    });
    console.log('----- prototype')
    Object.keys(Student.prototype).forEach((key) => {
        console.log(key, typeof object[key]), object[key];
        if (typeof object[key] == 'function') {
            console.log('   --->', object[key]());
        } else {
            console.log('   --->', object[key]);
        }
    });
}
stud = new Student('s13938', 'Michal', 'Jaworski', [3, 4, 5, 5, 4, 5, 3]);
stud2 = new Student('s113938', 'Michal', 'Jaworski', [3, 4, 5, 5, 4, 5, 3]);

describe(stud);

console.log(stud.listOfClasses);
stud2.listOfClasses.push('2345');
console.log(stud.listOfClasses);
function Student(indexNumber, name, lastName, marksArray) {
    this.indexNumber = indexNumber;
    this.name = name;
    this.lastName = lastName;
    this.marksArray = marksArray;
    this.avg = function() {
        sum = 0;
        this.marksArray.forEach((m) => sum += m);
        return sum / this.marksArray.length;
    };
    this.fullName = function(){
        console.log(this.name + " " + this.lastName + " " + this.avg());
    }
}


function descObject(O){
	for(var w in O){
        console.log(w + " -> " + typeof(O[w])); 
	}
}

/*
function describe(object) {
    Object.keys(object).forEach((key) => {
        console.log(key, typeof object[key]), object[key];
        if (typeof object[key] == 'function') {
            console.log(' ->', object[key]());
        } else {
            console.log(' ->', object[key]);
        }
    })};
*/




student = new Student('s13938', 'Michal', 'Jaworski', [3, 4, 5, 5, 4, 5, 3, 4]);
student2 = new Student('s999999', 'Robert', 'Lewandowski', [3, 4, 5, 5, 4, 5, 3, 4, 3, 3, 3, 3, 3]);
descObject(student);
console.log("-------------------------------------------------------");
descObject(student2);

student.fullName();
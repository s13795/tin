function fibFunction(p){
    if (p==0) return 0;
    if (p==1) return 1;
   
  return fibFunction(p-1) + fibFunction(p-2);

}

console.log(fibFunction(1));
console.log(fibFunction(2));
console.log(fibFunction(3));
console.log(fibFunction(4));
console.log(fibFunction(5));
console.log(fibFunction(6));
console.log(fibFunction(7));
console.log(fibFunction(8));
console.log(fibFunction(9));
console.log(fibFunction(10));

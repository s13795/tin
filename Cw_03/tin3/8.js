function secondFunction(p){
    p.sort((a, b) => a - b); 

    var prev = p[0];
    for(i=1;i<p.length;i++){
        if(prev == p[i]) {
            p.splice(i,1);
            i--;
        } else{
            prev=p[i];
        }
    }

    return p[1] + " " + p[p.length-2];

}

console.log(secondFunction([60,7,8,1,1,2,90,90,3,4,5]));
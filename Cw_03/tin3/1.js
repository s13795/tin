function powerFunction(p) {
    if (p==0) return 1;
    if (p==1) return 1;

    var wynik=1;
    for (i=1; i<=p;i++){
        wynik= wynik*i;
    }

    return wynik;  
}

function powerFunctionR(p){
    if (p==0) return 1;
    if (p==1) return 1;
    return p * powerFunctionR(p-1);
}



console.log(powerFunction(5));
console.log(powerFunctionR(5));
object = {
	indexNumber: 's13938',
    name: 'Michal',
    lastName: 'Jaworski',
    marksArray: [3,4,5,5,4,5,3,5,5],
    avg: function() {
    	sum = 0;
    	this.marksArray.forEach((m) => sum += m);
    	return sum / this.marksArray.length;
    },
    fullName: function() {
    	return this.name + ' ' + this.lastName;
    }
}


function descObject(O){
	for(var w in O){
		console.log(w + " -> " + typeof(O[w]));
	}
}

/*
function describe(object) {
    Object.keys(object).forEach((key) => {
		console.log(key, typeof object[key]), object[key];
		
        if (typeof object[key] == 'function') {
            console.log('->', object[key]());
        } else {
            console.log('->', object[key]);
		}
		
    })};
*/

descObject(object);
//describe(object);
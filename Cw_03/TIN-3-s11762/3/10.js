function binarySearch(array, value) {
    start = 0
    stop = array.length - 1
    m = Math.floor((start + stop) / 2)

    while (array[m] !== value && start < stop) {
        if (value < array[m]) {
            stop = m - 1
        } else {
            start = m + 1
        }

        m = Math.floor((start + stop) / 2)
    }

    return (array[m] !== value) ? -1 : m
}

array = [];
for (i = 0; i < 50; i++) {
  array.push(Math.floor((Math.random() * 1000)));
}
number = array[25];
array.sort((a,b) => a - b);
console.log(array, number);
console.log(binarySearch(array, number));
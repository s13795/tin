function longest(string) {
    sorted = string.split(' ').sort((a, b) => b.length - a.length);
    max = sorted[0].length;
    return sorted.filter((a) => max == a.length);
}

string = 'ala ma kota i mysz';
console.log(string + ' -> ' + longest(string));
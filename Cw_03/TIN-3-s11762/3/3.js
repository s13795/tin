function pali(string) {
    string = string.replace(/\s/g, '').toLowerCase();
    return string == string.split('').reverse().join('');
}

string = 'Kobyła ma mały bok';
result = pali(string);

console.log('Ciąg: ', string)
result ? console.log('jest palindromem') : console.log('nie jest palindromem');
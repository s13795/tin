function isPrime(n) {
    if (n < 2) return false;
    if (n % 2 == 0) return (n == 2);
    if (n % 3 == 0) return (n == 3);
    var m = Math.sqrt(n);
    for (var i = 5; i <= m; i += 6) {
        if (n % i == 0) return false;
        if (n % (i + 2) == 0) return false;
    }
    return true;
}

[2, 3, 4, 5, 6, 7, 8, 9, 100000001].forEach((i) => {
    console.log(i + ' -> ' + isPrime(i));
})
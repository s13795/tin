function type(a) {
    return typeof a;
}

['a', 1, { test: 1 }, [1, 2, 3]].forEach((a) => {
    console.log(a, 'is a', type(a));
})
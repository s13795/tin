function pow1(n) {
    result = 1;
    for (i = 2; i <= n; i++) {
        result *= i;
    }
    return result;
}

function pow2(n, currVal) {
    if (n < 2) return currVal || 1;
    if (!currVal) return pow2(n - 1, n);
    return pow2(n - 1, currVal * n);
}

function test(func) {
    console.log(func)
    console.log('silnia 0: ', func(0));
    console.log('silnia 1: ', func(1));
    console.log('silnia 2: ', func(2));
    console.log('silnia 3: ', func(3));
    console.log('silnia 4: ', func(4));
    console.log('silnia 5: ', func(5));
}

test(pow1);
test(pow2);
function amountTocoins(amount, coins) {
    result = [];
    coinIndex = 0;
    coins.sort((a, b) => b - a);
    while (amount) {
        currCoin = coins[coinIndex];
        if (amount >= currCoin) {
            result.push(currCoin);
            amount -= currCoin;
        } else {
            coinIndex++;
        }
    }
    return result;
}

console.log(amountTocoins(46, [25, 10, 5, 2, 1]));
object = {
	index: 's11762',
    name: 'Adrian',
    lastName: 'Powierża',
    marks: [3,4,5,5,4,5,3],
    avg: function() {
    	sum = 0;
    	this.marks.forEach((m) => sum += m);
    	return sum / this.marks.length;
    },
    fullName: function() {
    	return this.name + ' ' + this.lastName;
    }
}

function describe(object) {
	Object.keys(object).forEach( (key) => {
		console.log(key, typeof object[key]), object[key];
		if (typeof object[key] == 'function') {
			console.log('   --->', object[key]());
		} else {
			console.log('   --->', object[key]);
		}
	});
}

describe(object);
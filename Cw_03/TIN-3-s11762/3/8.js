function wtf(array) {
    sorted = array.sort();
    min = sorted[0];
    max = sorted.reverse()[0];
    sorted = sorted.filter((a) => a != min && a != max);
    return [sorted[0], sorted.reverse()[0]];
}
array = [1, 1, 2, 3, 4, 4, 4, 4];
console.log(array + ' -> ' + wtf([1, 2, 3, 4]));
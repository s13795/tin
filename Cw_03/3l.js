function Student(imie, nazwisko, nrIndeksu, oceny)
{
this.imie = imie;
this.nazwisko = nazwisko;
this.nrIndeksu = nrIndeksu;
this.oceny = oceny;
this.printStudent = function() 
	{
		sum = 0;
		this.oceny.forEach((m) => sum += m);
		avg = sum / oceny.length;
		return imie+"\n"+nazwisko+"\nSrednia: "+avg.toFixed(2);	
	}
};

var someStud = new Student('Jan', 'Nowak', 12023, [3,5,5,4,3,5])

console.log(someStud.printStudent());
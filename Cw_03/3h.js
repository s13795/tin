function get2ndTopBottom(arr) {
	arr.sort();
  return "Array: "+arr+
  "\nThe second lowest value is "+arr[1]+
  "\nThe second highest value is "+arr[arr.length-2];
}

var numbers = [23, 31, 67, 84, 34, 52, 12, 1, 99]

console.log(get2ndTopBottom(numbers));
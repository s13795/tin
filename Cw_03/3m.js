var Student = 
{
listaPrzedmiotow : ['TIN', 'MIW', 'CMS']
};


function StudentExtended (imie, nazwisko, nrIndeksu)
{
	// Student.call(this);
	this.imie = imie;
	this.nazwisko = nazwisko;
	this.nrIndeksu = nrIndeksu;
}

StudentExtended.prototype = Object.create(Student);

var StudentX = new StudentExtended('Jan','Filipkowski',13795);

console.log('Student: '+StudentX.imie+' '+StudentX.nazwisko+'; nr indeksu: '+StudentX.nrIndeksu+'\nLista przedmiotow:\n'+StudentX.listaPrzedmiotow);
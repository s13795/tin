var randomCar =
{
brand: "Skoda",
model: "Octavia",
validInspection: true	,
ifValidInspection: function(){return this.validInspection;},
showCarInfo: function(){return this.brand+" "+this.model;}
};

function describe(O){
	for(var w in O){
		console.log(w + " type of " + typeof(O[w]));
	}
}

describe(randomCar);
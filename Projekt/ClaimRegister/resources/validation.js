//  general validation of Claimer and Perpetrator data
function validate () {

    var result = true;

    if(document.getElementById("Owner").value.length == document.getElementById("Owner").value.replace(" ","").length || document.getElementById("Owner").value.length == 0){
        document.getElementById("Owner").style.border = "2px solid red"
        document.getElementById("OwnerHelp").innerHTML = "Imię i nazwisko nie może być puste.";
        result = false;
    }
    else {
        document.getElementById("Owner").style.border = "2px solid green"
        document.getElementById("OwnerHelp").innerHTML = "";
    }

    if(document.getElementById("IDnumber").value.length != 16){
        document.getElementById("IDnumber").style.border = "2px solid red"
        document.getElementById("IDnumberHelp").innerHTML = "Numer prawa jazdy powinien być w następującym formacie: XXXXX/XX/XXXXXXX gdzie X to dowolne cyfry.";
        result = false;
    }
    else {
        document.getElementById("IDnumber").style.border = "2px solid green"
        document.getElementById("IDnumberHelp").innerHTML = "";
    }

    if(document.getElementById("CarPlate").value.replace(" ","").length != 7){
        document.getElementById("CarPlate").style.border = "2px solid red"
        document.getElementById("CarPlateHelp").innerHTML = "Numer rejestracyjny pojazdu składa się z 7 znaków.";
        result = false;
    }
    else {
        document.getElementById("CarPlate").style.border = "2px solid green"
        document.getElementById("CarPlateHelp").innerHTML = "";
    }
    return result;
}

//  validation of Claimer
function validateClaimer(){

    var result = validate();

    if(result){
        document.getElementById("ClaimerData").submit();
        }
}

//  validation of Perpetrator
function validatePerpetrator(){

    var result = validate();

    if(document.getElementById("Driver").value.length == document.getElementById("Driver").value.replace(" ","").length || document.getElementById("Driver").value.length == 0){
        document.getElementById("Driver").style.border = "2px solid red"
        document.getElementById("DriverHelp").innerHTML = "Imię i nazwisko kierującego nie może być puste.";
        result = false;
    }
    else {
        document.getElementById("Driver").style.border = "2px solid green"
        document.getElementById("DriverHelp").innerHTML = "";
    }

    var pattern = /[A-Z]{4}[0-9]{6}/;

    if(pattern.test(document.getElementById("InsuranceNo").value) == false || document.getElementById("InsuranceNo").value.replace(" ","").length !=10){
        document.getElementById("InsuranceNo").style.border = "2px solid red"
        document.getElementById("InsuranceNoHelp").innerHTML = "Numer polisy powinien zawierać 4 litery oraz 6 cyfr.";
        result = false;
    }
    else {
        document.getElementById("InsuranceNo").style.border = "2px solid green"
        document.getElementById("InsuranceNoHelp").innerHTML = "";
    }   

    if(result){
        document.getElementById("PerpetratorData").submit();
    }
    
}
//  validation of Claim Description
function validateClaimDesc(){
    var result = true;
    
    var inputYear = Number.parseInt(document.getElementById("Date").value.split('-')[0]);
    if(inputYear<(new Date().getFullYear())-1 || inputYear>new Date().getFullYear()){
        document.getElementById("Date").style.border = "2px solid red"
        document.getElementById("DateHelp").innerHTML = "Podana data jest błędna, wprowadź poprawną datę.";
        result = false;
    }
    else {
        document.getElementById("Date").style.border = "2px solid green"
        document.getElementById("DateHelp").innerHTML = "";
    }

    if(document.getElementById("Description").value.length < 50){
        document.getElementById("Description").style.border = "2px solid red"
        document.getElementById("DescriptionHelp").innerHTML = "Opis zdarzenia powinien zawierać przynajmniej 50 znaków.";
        result = false;
    }
    else {
        document.getElementById("Description").style.border = "2px solid green"
        document.getElementById("DescriptionHelp").innerHTML = "";
    }

    if(document.getElementById("ClaimedAmount").value.length == 0){
        document.getElementById("ClaimedAmount").style.border = "2px solid red"
        document.getElementById("ClaimedAmountHelp").innerHTML = "Kwota roszczenia nie może być pusta.";
        result = false;
    }
    else {
        document.getElementById("ClaimedAmount").style.border = "2px solid green"
        document.getElementById("ClaimedAmountHelp").innerHTML = "";
    }
   
    if(result){
        document.getElementById("ClaimDescData").submit();
    }
    
}
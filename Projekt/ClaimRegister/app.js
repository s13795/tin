var express = require('express');
var app = express();
var parser = require('body-parser');

app.set('view engine', 'ejs');
app.use(parser.urlencoded({ extended: false }));
app.use(parser.json());
app.use(express.static(__dirname + '/resources'));
 
app.get('/', function(req, res) {

    res.render('pages/index');

});

var ClaimerData = {
    Owner : "",
    Driver : "",
    IDnumber : "",
    CarPlate : "",
}
var PerpetratorData = {
    Owner : "",
    Driver : "",
    IDnumber : "",
    CarPlate : "",
    InsuranceNo : "",
}
var ClaimDescData = {
    Date : "",
    Description : "",
    ClaimedAmount : ""
}

app.get('/ClaimerForm', function(req, res) {
    
    res.render('pages/ClaimerForm',{
        ClaimerData
    });

});

app.post('/ClaimerForm', function(req, res) {
    
    ClaimerData["Owner"] = req.body.Owner;
    ClaimerData["Driver"] = req.body.Driver;
    ClaimerData["IDnumber"] = req.body.IDnumber;
    ClaimerData["CarPlate"] = req.body.CarPlate;
    
    res.render('pages/PerpetratorForm',{
        PerpetratorData
    });

});

app.get('/PerpetratorForm', function(req, res) {

    res.render('pages/PerpetratorForm',{
        PerpetratorData
    });

});

app.post('/PerpetratorForm', function(req, res) {
    
    PerpetratorData["Owner"] = req.body.Owner;
    PerpetratorData["Driver"] = req.body.Driver;
    PerpetratorData["IDnumber"] = req.body.IDnumber;
    PerpetratorData["CarPlate"] = req.body.CarPlate;
    PerpetratorData["InsuranceNo"] = req.body.InsuranceNo;
  
    res.render('pages/ClaimDescForm',{
        ClaimDescData
    });

});

app.get('/ClaimDescForm', function(req, res) {

    res.render('pages/ClaimDescForm',{
        ClaimDescData
    });

});

app.post('/ClaimDescForm', function(req, res) {
        
    ClaimDescData["Date"] = req.body.Date;
    ClaimDescData["Description"] = req.body.Description;
    ClaimDescData["ClaimedAmount"] = req.body.ClaimedAmount;

    res.render('pages/resultData',{
        ClaimerData,
        PerpetratorData,
        ClaimDescData
    });

});

app.get('/SubmitSuccess', function(req, res) {

    ClaimerData = {};
    PerpetratorData = {};
    ClaimDescData = {};
    res.render('pages/SubmitSuccess');
    
});

app.listen(9999);
console.log('9999 is on.');
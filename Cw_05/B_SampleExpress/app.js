var express = require('express');
var app = express();
var parser = require('body-parser');

app.set('view engine', 'ejs');
app.use(parser.urlencoded({ extended: false }));
app.use(parser.json());
 
app.get('/', function(req, res) {
    res.render('pages/index');
});

app.get('/hello', function(req, res) {
    res.render('pages/hello');
});

app.get('/form', function(req, res) {
    res.render('pages/form');
});

app.post('/form', function(req, res) {
    console.log(req.body);
    console.log(req.body.lastname);
    var Person = {
        first : req.body.firstname,
        last : req.body.lastname,
        pass : req.body.password,
    } 
    res.render('pages/formdata',{
        PersonData : Person
    });
});

app.post('/jsondata', function(req, res) {
    var Person = {
        first : req.body.first,
        last : req.body.last,
        pass : req.body.pass
    } 
    res.render('pages/jsondata',{
        PersonData : Person
    });
});

app.listen(9999);
console.log('9999 is the one.');